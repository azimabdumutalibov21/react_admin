import React from "react";
import { Link, NavLink,  } from "react-router-dom";
import sidebarNav from "../../routers/sidebarNav";
import config from "../../configs/default";
const Sidebar = () => {

  return (
    <div className="max-w-sm h-full sticky top-0 z-50">
      <div
        className=" h-screen w-000 m-0 flex flex-col bg-white  content-center sticky top-0
           shadow text-secondary items-center "
      >
        <Link to={'/'}>
            <div className="flex-col m-2 justify-center content-center items-center w-00 ">
          <div className="text-4xl flex mb-10 font-bold justify-center items-center  text-white rounded-3xl text-center w-14 h-11 cursor-pointer">
            <img src={config.d} alt="" className='w-20' />
          </div>
        </div>

        </Link>

        <div className="flex-col m-3 justify-center content-center w-400  bg-gray-200 rounded mb-auto">
        {sidebarNav.map((nav, index) => (
             <NavLink style={({isActive}) => ({background: isActive ? 'cyan': 'red' ,borderRadius:'4px'})}
             to={nav.link}
             key={`nav-${index}`}
            
           >
               <div className="p-3  rounded bg-gradient-to-r  hover:from-cyan-500 hover:to-blue-500 text-indigo-500 group-hover:text-indigo-400">
                   {nav.icon}
               </div>
              
           </NavLink>
        ))}
        </div>
          <div className="mb-3 cursor-pointer">
            <img src={config.little} alt="" />
            <img className="mt-4 " src={config.setting} alt="" />
        <Link to={"/login"}>
            <div className="mt-4">
              <img src={config.ellipse} alt="" />
            </div>
        </Link>
          </div>
      </div>
    </div>
  );
};

export default Sidebar;
