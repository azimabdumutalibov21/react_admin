import { ReactComponent as buyurtma } from "../assets/icons/9.svg";
import { ReactComponent as filial } from "../assets/icons/4.svg";
import { ReactComponent as hisobot } from "../assets/icons/5.svg";
import { ReactComponent as hodimlar } from "../assets/icons/6.svg";
import { ReactComponent as settings } from "../assets/icons/7.svg";
import { ReactComponent as mahsulotlar } from "../assets/icons/8.svg";
import { ReactComponent as kategoriya } from "../assets/icons/2.svg";
import { ReactComponent as mijozlar } from "../assets/icons/3.svg";

//Components

// Containers
import GenericContainer from "@containers/Generic/index";
import OrderContainer from "@containers/Order/index";
import DashboardContainer from "@containers/Dashboard/index";
import CompanyContainer from "@containers/Company/index";
import TableContainer from "@containers/Table/index";
import HomeContainer from "@containers/Home/index"
import StarContainer from "@containers/Star/index"
import HistoryContainer from "@containers/History/index"
import SettingsContainer from "@containers/Settings/index"
// import AuthContainer from "@containers/Auth_container/index";
// // import MainContainer from "@containers/Main_container/index";
// import EmptyContiner from "@containers/Empty_container/index";
// // pages
// import P404 from "@pages/exceptions/PageNotFound";
import Dashboard from "@pages/dashboard/Dashboard";
import Клиенты from '@pages/Клиенты';
// import Login from "@pages/login";
export const routes = [
  {
    id: 1,
    path: "/",
    title: "dashboard",
    hideChildren: false,
    component: DashboardContainer,
    Icon: buyurtma ,


    children: [
      {
        path: "/main",
        title: "Dashboard",
        component: Dashboard,
        Icon: mahsulotlar,

        // children: [],
      },
    ],
  },
  {
    id: 2,

    path: "/order",
    title: "order",
    hideChildren: false,
    component: OrderContainer,
    Icon: kategoriya,

    children: [
      {
        path: "/order/main",
        title: "Клиенты",
        component: Клиенты,
        Icon: mahsulotlar,

        children: [],
      },
    ],
  },
  {
    id: 3,

    path: "/company",
    title: "company",
    hideChildren: false,
    component: CompanyContainer,
    Icon: mijozlar,

    children: [
      {
        path: "courier",
        title: "Courier",
        component: GenericContainer,
        Icon: mahsulotlar,

        children: [],
      },
    ],
  },
  {
    id: 4,

    path: "/filial",
    title: "filial",
    hideChildren: false,
    component: TableContainer,
    Icon: filial,

    children: [
      {
        path: "courier",
        title: "Courier",
        component: GenericContainer,
        Icon: mahsulotlar,

        children: [],
      },
    ],
  },
  {
    id: 5,

    path: "/home",
    title: "order",
    hideChildren: false,
    component: HomeContainer,
    Icon: hisobot,

    children: [
      {
        path: "courier",
        title: "Courier",
        component: StarContainer,
        Icon: mahsulotlar,

        children: [],
      },
    ],
  },
  {
    id: 6,

    path: "/star",
    title: "order",
    hideChildren: false,
    component: StarContainer,
    Icon: hodimlar,

    children: [
      {
        path: "courier",
        title: "Courier",
        // component: Courier,
        Icon: mahsulotlar,

        children: [],
      },
    ],
  },
  {
    id: 7,

    path: "/history",
    title: "order",
    hideChildren: false,
    component: HistoryContainer,
    Icon: settings,

    children: [
      {
        path: "courier",
        title: "Courier",
        // component: Courier,
        Icon: mahsulotlar,

        children: [],
      },
    ],
  },
  {
    id: 8,

    path: "/settings",
    title: "order",
    hideChildren: false,
    component: SettingsContainer,
    Icon: mahsulotlar,

    children: [
      {
        path: "courier",
        title: "Courier",
        // component: Courier,
        Icon: mahsulotlar,

        children: [],
      },
    ],
  },

  // {
  //     path: '/login',
  //     title: 'login',
  //     hideChildren: false,
  //     component: AuthContainer,
  //   Icon: kategoriya,

  //     children: [
  //         {
  //   Icon: filial,
  //   path: 'auth',
  //             title: 'auth',
  //             component: Login,
  //             children: []
  //         }
  //     ]
  // },
  // {
  //   Icon: mijozlar,
  //   path: '/exception',
  //     title: 'exception',
  //     hideChildren: false,
  //     component: EmptyContiner,
  //     children: [
  //         {
  //             path: '404',
  //   Icon: hisobot,
  //   title: 'Not found',
  //             component: P404,
  //             children: []
  //         }
  //     ]
  // }
];
