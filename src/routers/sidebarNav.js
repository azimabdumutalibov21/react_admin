import config from "../configs/default";
const sidebarNav = [
  {
    link: "/cart",
    section: "home",
    icon: <img src={config.cart} alt=""/>,
    text: "О компании",
  },
  {
    link: "/user",
    section: "orders",
    icon: <img src={config.men} alt=""/>,
    text: "Филиалы компаний",
  },
  {
    link: "/circle/menu1",
    section: "products",
    icon: <img src={config.circle} alt=""/>,
    text: "Регионы",
  },
  {
    link: "/",
    section: "products",
    icon: <img src={config.nine} alt=""/>,
    text: "Регионы",
  },
];

export default sidebarNav;
