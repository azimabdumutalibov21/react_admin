// import logo from './logo.svg';
import "./App.css";
import './assets/boxicons-2.1.1/css/boxicons.min.css'
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import { routes } from "./routers";
import Cart from "./pages/Order/Cart/Cart";
import User from './pages/Order/users/index'
import Circle from './pages/Order/Circle/Circle'
import Auth from './containers/Auth_container'
import Menu1 from './pages/Order/Circle/SecondSidebar/Select/Menu/index'
import Menu2 from './pages/Order/Circle/SecondSidebar/Select/Menu2/index'
// import EmptyContainer from './containers/Empty_container'
// import Sidebar from "./components/Sidebar";
// async permission = []
function App() {
  console.log(routes);
  // const generator = ({ children }) => {
  //   if (!children.length || children) {
  //     return
  //   }
  //   generator()
  // }
  return (
    <div className="flex">
      <Router>
        {/* <Routes>
          {routes.map(({ component: Component, path, children }, i) => (
            <Route key={i} path={path} element={<Sidebar />} >
                 {children
                ? children.map((el, f) => (
                    <Route
                      path={el.path}
                      key={f + 40}
                      element={<el.component />}
                      />
                      ))
                      : ""}
            </Route>
          ))}
        </Routes> */}
        <Routes>
       
          {routes.map(({ component: Component, path, title, children }, i) => (
            <Route key={i} path={path} element={<Component />}>
              {children
                ? children.map((el, f) => (
                  <Route
                  path={el.path}
                  key={f + 40}
                  element={<el.component />}
                  />
                  ))
                  : '' }
            </Route>
          ))}
        </Routes>
        <Routes>
       <Route path='/cart' element={<Cart/>}/>
      <Route path='/user' element={<User/>}/>
      <Route path='/circle/menu1' element={<Circle/>}/>
      <Route path='/circle/menu2' element={<Menu1/>}/>
      <Route path='/circle/menu3' element={<Menu2/>}/>
       <Route path='/login' element={<Auth/>}/>
       
    {/* <Route path="*" element={<EmptyContainer/>} /> */}
      </Routes>
      </Router>
    </div>
  );
}

export default App;
