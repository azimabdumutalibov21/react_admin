import React from 'react'
import Order from '../../components/Order'
import Sidebar from '../../components/Sidebar'

export default function index() {
    return (
        <div className='flex w-full'>
            <Sidebar/>
            <Order/>
        </div>
    )
}
