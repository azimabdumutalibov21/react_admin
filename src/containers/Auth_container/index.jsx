// import { Outlet } from "react-router-dom";
import config from "../../configs/default";
import { LockClosedIcon,UserIcon, EyeIcon } from "@heroicons/react/solid";
import { useForm } from "react-hook-form";



export default function Login({ logo, title }) {
    const {register, handleSubmit} = useForm();
    
    const onSubmit = handleSubmit((data)=>{
        console.log(data);
    })
  return (
    <div className="flex w-full">
        <div></div>
      <div className="w-full flex flex-col justify-center align-middle h-screen bg-[#0067F4] relative">
                        <div class="relative">
  <div class="absolute bottom-40 left-40 ...">
  <p class="text-white-900/100 text-5xl text-white font-bold" >Bootcamp</p>
  </div>
</div>
        <div className="flex  justify-center h-30">
 

          <img src={config.logo} alt="dasf" />
 
        </div>
        <div className="text-4xl">{config.welcome}</div>
        
      </div>
      <div className="w-full h-screen bg-[#E5E5E5]">
        {/* <Outlet /> */}
        
        <div className="min-h-screen bg-gray-50 flex flex-col justify-center ">
          <div className="max-w-2xl w-full mx-auto ">
            <div className="text-center font-medium text-xl"></div>
            <div className="text-3xl font-bold text-gray-900 mt-2 start">
            Вход в платформу
            </div>
          </div>
          <div className="max-w-2xl w-full mx-auto mt-8   p-90">
            <form action="" className="space-y-6" onSubmit={onSubmit}>

            <div className="mt-1 relative rounded-md shadow-sm">
            <label htmlFor="" className="text-sm  font-bold text-gray-600 blold">Имя пользователья *</label>
                
        <div className="absolute inset-y-16 left-0 pl-3 flex items-center pointer-events-none">
                  <UserIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
        </div>
        
        <input
          type="text"
          className="w-full  p-4 border border-gray-300 rounded mt-1 pl-10 pr-12  "
          placeholder="Введите e-mail"
          autocomplete autofocus required
        />
        
      </div>


              <div className="mt-1 relative rounded-md shadow-sm">
            <label htmlFor="" className="text-sm  font-bold text-gray-600 blold">Имя пользователья *</label>
                
        <div className="absolute inset-y-14 left-0 pl-3 flex items-center pointer-events-none">
                  <LockClosedIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
        </div>
        
        <div className="absolute inset-y-16 left-80 pl-80 flex items-center pointer-events-none">
                  <EyeIcon className="h-5 w-5 text-indigo-500 group-hover:text-indigo-400" aria-hidden="true" />
        </div>

        <input
          type="text"
          name="price"
          id="price"
          className="w-full pl-10 p-4 border border-gray-300 rounded mt-1 pl-10 pr-12  "
          placeholder="Введите пароль"
          autocomplete autofocus required
        />
        
      </div>

              <div className="flex items-center justify-between">
                  <div className="flex item-center">
                      <input ref={register()} name="password" type="checkbox" className="h-4 w-4 text-blue-300 rounded" autocomplete autofocus required />
                      <label htmlFor="" className="ml-2 text-sm text-gray-600">Запомнить меня</label>
                  </div>
                  
              </div>
              <button type="submit" className="w-full py-4 px-4 bg-blue-600 hover:bg-blue-700 rounded-md text-white text-sm">Войти</button>
            </form>

         
          </div>
        </div>
      </div>
    </div>
  );
}

           