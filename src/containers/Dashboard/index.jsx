import React from 'react'
import Dashboard from '../../components/Dashboard'
import Sidebar from '../../components/Sidebar'
export default function index() {
    return (
        <div className='w-full flex sticky top-0 z-auto'>
            
<Sidebar/>
            <Dashboard/>
        </div>
    )
}
