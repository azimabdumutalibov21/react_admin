import React from "react";
import { Link, } from "react-router-dom";
import configs from "../../../configs/default";
import sidebarNav from "../../../routers/sidebarNav";

export const Topbar = () => {
 

  
  return (
    <div>
      <div className="flex  border h-14 w-full  content-center items-center sticky top-0 z-50 bg-white">
        <div className="flex w-full text-2xl hover:text-blue-500 ml-10 font-semibold text-gray-700">
          <img className="cursor-pointer" src={configs.arrow} alt="" />
          <img
            className="ml-3 mr-2 cursor-pointer"
            src={configs.file1}
            alt=""
          />
          <div className="text-xl text-gray-400 cursor-pointer  font-normal mr-3 hover:text-blue-500">
            Список компаний
          </div>
          <img className="cursor-pointer mr-3" src={configs.arrowLeft} alt="" />
          <img className="cursor-pointer mr-2" src={configs.file2} alt="" />
          <div className="text-xl font-normal "> L'Oréal</div>
        </div>

        <div className="border-l items-center h-full flex mr-4 text-green-500 text-xl cursor-pointer">
          {" "}
          <div className="mr-3 ml-3 fill-red">+</div> Добавить
        </div>
      </div>
      <div className="flex  border h-14 w-full  content-center items-center sticky top-0 z-50 bg-white">
        <div className="flex w-full text-2xl  ml-10 font-semibold text-gray-700">
          <div className= "flex text-lg font-normal ">
          {sidebarNav.map((nav, index) => (
          <Link className='mr-5 text-decoration-none '
               to={nav.link}
            key={`nav-${index}`}
          
          >
              
              <div className="">
                  {nav.text}
              </div>
          </Link>
        ))}
          </div>
        </div>
      </div>
    </div>
  );
};
