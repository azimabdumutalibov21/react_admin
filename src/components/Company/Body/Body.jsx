import React from 'react'
import App from './table/App'
import Paginate from './Paginate'
export default function Body() {
    return (
        <div className=' bg-blue-200  w-full mb-0.5   px-9  py-6 h-full'>
           <div className="flex h-full">
<div className='flex flex-col w-full  h-full bg-white'>
    <div className='h-5/6'>

<App/>

    </div>
     <Paginate/>
</div>

           </div>
        </div>
    )
}
