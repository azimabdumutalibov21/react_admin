import React from 'react'
import Body from './Body/Body'
import { Topbar } from './Topbar'
export default function Company() {
    return (
        <div className='w-full'>
         <Topbar/>
         <Body/>
        </div>
    )
}
