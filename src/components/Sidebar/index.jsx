import React from "react";
import { Link, NavLink } from "react-router-dom";
import config from "../../configs/default";
import { routes } from "../../routers/index";
export const Sidebar = () => {
  
  return (
    <div className="max-w-sm sticky   bottom-0 top-0 z-auto">
      <div
        className=" h-screen w-000 m-0 flex flex-col bg-white  content-center sticky top-0 z-50
           shadow text-secondary items-center "
      >
        <div className="flex-col m-2 justify-center content-center items-center w-00 ">
          <div className="text-4xl flex mb-10 font-bold justify-center items-center  text-white rounded-3xl text-center w-14 h-11 cursor-pointer">
            <img src={config.d} alt="" className='w-20' />
          </div>
        </div>

        <div className="flex-col m-3 justify-center content-center w-400  bg-gray-200 rounded mb-auto">
          {routes.map(({ id, Component, title, path, Icon, icon }, i) => {
            return (

              <NavLink  style={({isActive}) => ({background: isActive ? 'cyan': '' ,borderRadius:'4px'})}
              className="m-0 flex"
              key={i}
              to={path}
       
            >
              <div className=" p-3  rounded bg-gradient-to-r  hover:from-cyan-500 hover:to-blue-500   ">
                <Icon  />
              </div>
            </NavLink>

            );
          })}
        </div>
        <Link to={"login"}>
          <div className="mb-3 ">
            <img src={config.little} alt="" />
            <div className="mt-4">
              <img src={config.ellipse} alt="" />
            </div>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default Sidebar;
// style={({isActive}) => ({background: isActive ? 'black': 'white'})}