import React from 'react'
import { Link } from 'react-router-dom'

export const Topbar = () => {
    return (

        <div className='flex  border h-14 w-full  content-center items-center sticky top-0 z-50 bg-white' >
          <Link to={'login'} className='w-full '>
          
          <div className='w-full text-2xl hover:text-blue-500 ml-10 font-semibold text-gray-700'>Клиенты</div> 
          </Link>
          <div className='border-l items-center h-full flex mr-4 text-green-500 text-xl cursor-pointer'> <div className='mr-3 ml-3 fill-red'>+</div> Добавить</div>
        </div>
    )
}
