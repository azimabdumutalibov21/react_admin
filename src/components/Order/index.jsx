import React from 'react'
import Body from './Body'
import { Topbar } from './Topbar'

export default function Order() {
    return (
        <div className='flex-col w-full'>
            
               <Topbar/>
            <Body/> 
        </div>
    )
}
