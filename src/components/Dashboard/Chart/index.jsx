import React from 'react'
import {  XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, AreaChart, Area,  } from 'recharts';
const pdata = [
  {
    name: 'Понедельник',
    student: 13,
    fees: 10
  },
  {
    name: 'Вторник',
    student: 15,
    fees: 12
  },
  {
    name: 'Среда',
    student: 5,
    fees: 10
  },
  {
    name: 'Четверг',
    student: 10,
    fees: 5
  },
  {
    name: 'Пятница',
    student: 9,
    fees: 4
  },
  {
    name: 'Суббота',
    student: 10,
    fees: 8
  }, {
    name: 'Воскрессенье',
    student: '',
    fees: 7
  },
];

export default function index() {
    return (
        <div className=" text-black text-center py- rounded-lg bg-white  mr-6   ml-6  mb-8  w-full">
           <>
      {/* <h1 className="chart-heading">Line Chart</h1>
      <ResponsiveContainer width="100%" aspect={3}>
        <LineChart data={pdata} width={500} height={300} margin={{ top: 5, right: 200, left: 20, bottom: 5 }}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" interval={'preserveStartEnd'} tickFormatter={(value) => value + " Programming"} />
          <YAxis />
          <Tooltip contentStyle={{ backgroundColor: 'yellow' }} />
          <Legend />
          <Line type="monotone" dataKey="student" stroke="red" activeDot={{ r: 8 }} />
          <Line type="monotone" dataKey="fees" stroke="green" activeDot={{ r: 8 }} />
        </LineChart>
      </ResponsiveContainer> */}

      <h1 className="chart-heading">Area Chart</h1>
      <ResponsiveContainer width="95%" aspect={3}>
          
       
        <AreaChart
          width={700}
          height={300}
          data={pdata}
          margin={{
            top: 5,
            right: 10,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Area type="monotone" dataKey="student" stroke="#8884d8" fill="#8884d8" />
        </AreaChart>
      </ResponsiveContainer>

      {/* <h1 className="chart-heading">Bar Chart</h1>
      <ResponsiveContainer width="100%" aspect={3}>
        <BarChart
          width={500}
          height={300}
          data={pdata}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="student" fill="#8884d8" />
          <Bar dataKey="fees" fill="#82ca9d" />
        </BarChart>
      </ResponsiveContainer> */}
    </>
        </div>
    )
}
