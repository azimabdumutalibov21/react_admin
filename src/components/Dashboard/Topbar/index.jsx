import React from "react";
// import { Outlet } from 'react-router-dom'

export default function Topbar() {
  return (
    <>
      <div className="flex bg-white  border h-14 w-full  content-center items-center sticky top-0 z-50 ">
      <div className="w-full text-2xl ml-10 font-semibold text-gray-700">
        Dashboard
      </div>
    </div>
    </>
  
  );
}
