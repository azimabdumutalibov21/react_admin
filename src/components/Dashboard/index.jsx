import React from 'react'
import Body from './Body'
import  Topbar  from './Topbar'

export default function Dashboard() {
    return (
        <div className='w-full flex-col '>
     <Topbar/>
            <Body/>
           

        </div>
    )
}
