const people = [
  {
    Число:'591',
    Доставка:'213',

    number: '425',
     Самовывоз:'5932',
    name: '43432',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    Число:'31',
    Доставка:'451',

    number: '655',
    Самовывоз:'6311',
    name: '43432',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    Число:'721',
    Доставка:'267',

    number: '546',
    Самовывоз:'9342',
    name: '43432',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    Число:'831',
    number: '224',
    Доставка:'23',

    name: '43432',
    Самовывоз:'3566',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    Число:'932',
    number: '672',
    Самовывоз:'1004',
    Доставка:'523',
    name: '43432',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    Число:'562',
    number: '256',
    Самовывоз:'7002',
    Доставка:'63',

    name: '43432',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
  {
    Число:'245',
    number: '556',
    Самовывоз:'6002',
    Доставка:'51',

    name: '43432',
    title: 'Regional Paradigm Technician',
    department: 'Optimization',
    role: 'Admin',
    email: 'jane.cooper@example.com',
    image:
      'https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=256&h=256&q=60',
  },
    // More people...
  ]
  export default function Table() {
    return (
      <div className="flex flex-col  ">
        
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 ">
          
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8 ">
            
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg items-center justify-conter">
              <div className="min-w-full divide-y divide-gray-200 bg-gray-50 p-10">Общий отчет</div>
              <table className="min-w-full divide-y divide-gray-200">
                
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-20 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Число
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium  uppercase tracking-wider"
                    >
                      Доставка
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                      Доставка
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                   Самовывоз
                    </th>
                    <th scope="col" className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Агрегаторы
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {people.map((person) => (
                    <tr key={person.email}>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div className="flex items-center">
                          <div className="flex-shrink-0 h-10 w-10">
                          </div>
                          <div className="ml-4">
                            <div className="text-sm  ">{person.Число}</div>

                          </div>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div>{person.number}</div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div>{person.Доставка}</div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap ">{person.Самовывоз}</td>
                      <td className="px-6 py-4 whitespace-nowrap">
                        <div  className=" ">
                          <div>{person.Число}</div>
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }