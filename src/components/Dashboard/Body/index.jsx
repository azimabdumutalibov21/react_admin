import React from "react";
import config from "../../../configs/default";
import { userData } from "../../../utils/dummyData";
import Chart from "./chart/Chart";
import Shart from '../Chart'
import Table from "./Table";
export default function Body() {
  return (
    <div className=" bg-blue-200  w-full mb-0.5   px-9  py-6 h-full">
      <div className="flex h-full">
        <div className=" flex flex-col w-full  h-full ">
          <div className=" text-black  py- rounded-lg flex   h-32  mb-6  w-full">
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr-6 transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  24
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Филиалы
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer stroke-red ">
                <img className='stroke-red' src={config.home} alt="dasf" />
              </div>
            </div>
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr-6 transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  110,823
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Клиенты
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer stroc-red">
                <img src={config.user} alt="dasf"  />
              </div>
            </div>
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr-6 transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  112,314
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Подписчики
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer ">
                <img src={config.ball} alt="dasf" />
              </div>
            </div>
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr- transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  73
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Курьеры
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer ">
                <img src={config.car} alt="dasf" />
              </div>
            </div>
          </div>
          <div className="flex-col h-full">
            <div className=" flex  w-full">
              <div className="flex-col w-4/12">
              <div >
                  <Chart  data={userData} title='User Analytics' grid dataKey='Active User' className=''/>
                  </div>
                  <div >
                  <Chart  data={userData} title='User Analytics' grid dataKey='Active User' className=''/>
                  </div>  <div >
                  <Chart  data={userData} title='User Analytics' grid dataKey='Active User' className=''/>
                  </div>
              
              </div>

              <div className="w-full mr-6 flex-col">
                <div className=" text-black text-center py- rounded-lg flex rounded-lg bg-white w-full mr-6 items-center  ml-6  mb-6 items-center w-full">
                  <div className=" text-black text-center py- rounded-lg flex bg-blue-200   mb-  w-full">
                    <div className="flex rounded-lg bg-white w-full items-center h-52  mr-6"><img src={config.parcent} alt="" className="ml-12"/> <div className="ml-20 text-gray-400 text-2xl">Top Card <div className="text-5xl font-bold text-emerald-500">75%</div> </div></div>
                    <div className="flex rounded-lg bg-white w-full items-center h-52  "><img src={config.p40} alt="" className="ml-12"/> <div className="ml-20 text-gray-400 text-2xl">Average order<div className="text-5xl font-bold text-purple-600">40%</div> </div></div>
                    {/* <div className="rounded-lg bg-white w-full ">f</div> */}
                  </div>
                </div>
                {/* <div className=" text-black text-center py- rounded-lg bg-white  mr-6  h-4/6 ml-6  mb-8  w-full"> */}
                  <Shart/>
                {/* </div> */}
              </div>
            </div>
            <div className="">
              <Table/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
