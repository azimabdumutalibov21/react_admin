import "./chart.css";
import {
  LineChart,
  Line,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Area,
  
} from "recharts";
export default function Chart({title, data, dataKey, grid}) {
  // text-black text-center py- rounded-lg flex rounded-lg bg-white w-full mr-6  h-44  mb-6  w-full
  return (
    <div className="chart h-60">
      <h3 className="chartTitle">Total orders this month</h3>
      <ResponsiveContainer width="100%"  aspect={4 / 1}>
        <LineChart data={data} >
          {/* <XAxis dataKey="name" stroke="#5550bd" /> */}
          <Line type="monotone" dataKey={dataKey} stroke="#5550bd" fill="blue"/>
          <Area type="monotone" dataKey="student" stroke="#8884d8" fill="#8884d8" />

          <Tooltip />
    {grid && <CartesianGrid stroke="#e0dfdf" strokeDasharray='5 5'/>}
        </LineChart>
      </ResponsiveContainer>
    </div>
  );
}
