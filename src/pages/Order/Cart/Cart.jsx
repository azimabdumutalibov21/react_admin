import React from 'react';
import Sidebar from '../../../layouts/Sidebar';
import Topbar from './Topbar'
import Body from './Body'
export default function Cart() {
  return (
  <div className='flex w-full '>
      <Sidebar/>
      <div className='w-full flex-col '>
        <Topbar/>
      <Body/>
      </div>
      

  </div>
  
  );
}
