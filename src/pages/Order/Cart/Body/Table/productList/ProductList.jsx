import { DataGrid } from "@material-ui/data-grid";
// import { DeleteOutline } from "@material-ui/icons";
import config from '../../../../../../configs/default'
import { productRows } from "../../../../../../routers/dummyData";
import { useState } from "react";
export default function ProductList() {
  const [data, setData] = useState(productRows);

  const handleDelete = (id) => {
    setData(data.filter((item) => item.id !== id));
  };

  const columns = [
    { field: "id", headerName: "№", width: 90 },
    {
      field: "product",

      headerName: "Клиент",
      width: 260,
      renderCell: (params) => {
        return (
          <div className=" flex-col">
            <div className="h-5"> {params.row.name}</div>
            <div className="text-blue-500"> {params.row.img} </div>
          </div>
        );
      },
    },
    { field: "stock", headerName: "Ид.заказа", width: 160 },
    {
      field: "status",
      headerName: "Таймер",
      width: 160,
      renderCell: (params) => {
        return (
          <div className=" flex-col">
            <div className="flex justify-center items-center rounded-xl bg-emerald-200 text-emerald-600 w-20 h-10">
              {params.row.status}
            </div>
          </div>
        );
      },
    },
    {
      field: "price",
      headerName: "Курьер",
      width: 160,
    },
    {
      field: "Филиал",
      headerName: "Филиал",
      width: 200,
      renderCell: (params) => {
        return (
          <div className=" flex-col">
            <div className="h-5"> {params.row.Филиал}</div>
            <div className="text-blue-500"> {params.row.number} </div>
          </div>
        );
      },
    },
    {
      field: "Типдоставки",
      headerName: "Тип доставки",
      width: 170,
      renderCell: (params) => {
        return (
          <div className=" flex-col">
            <div className="flex justify-center items-center rounded-xl bg-amber-100 text-amber-500 w-20 h-10">
              {" "}
              {params.row.Типдоставки}{" "}
            </div>
          </div>
        );
      },
    },
    {
      field: "Ценазаказа",
      headerName: "Цена заказа",
      width: 170,
    },
    {
      field: "aдрессклиента",
      headerName: "Aдрессклиента",
      width: 340,
    },
    {
      field: "",
      headerName: "",
      width: 0,
      renderCell: (params) => {
        return (
          <>
            {/* <button className="productListEdit">Edit</button> */}
            <img src={config.Action} alt=""
              className="text-red-500 cursor-pointer"
              onClick={() => handleDelete(params.row.id)}
            />
          </>
        );
      },
    },
  ];
  return (
    <div className="w-full">
      <DataGrid
        className="w-full"
        rows={data}
        disableSelectionOnClick
        columns={columns}
        pageSize={10}
        checkboxSelection
      />
    </div>
  );
}
