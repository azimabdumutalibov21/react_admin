import React from 'react';
import ProductList from './productList/ProductList'
export default function index() {
  return (
  <div className='bg-blue-200  w-full mb-0.5   px-6  py-6 h-full'>
     <div className='flex-col bg-white w-full mb-0.5   px-9  py-6 h-full rounded-xl'>
         <div className='flex border-b-4 border-gray-200 pb-3 mb-6'>
         <div className='flex justify-center items-center mr-4 font-semibold text-gray-500 hover:text-blue-600 cursor-pointer'>Курьер в пути <div className='flex justify-center items-center ml-2 w-7 h-7 rounded-full bg-blue-200 '>10</div></div>
         <div className='flex justify-center items-center mr-4 font-semibold text-gray-500 hover:text-blue-600 cursor-pointer'>Новый  <div className='flex justify-center items-center ml-2 w-7 h-7 rounded-full bg-blue-200 '>12</div></div>
         <div className='flex justify-center items-center mr-4 font-semibold text-gray-500 hover:text-blue-600 cursor-pointer'>Оператор принял <div className='flex justify-center items-center ml-2 w-7 h-7 rounded-full bg-blue-200 '>12</div></div>
         <div className='flex justify-center items-center mr-4 font-semibold text-gray-500 hover:text-blue-600 cursor-pointer'>Загатовка<div className='flex justify-center items-center ml-2 w-7 h-7 rounded-full bg-blue-200 '>5</div></div>
         <div className='flex justify-center items-center mr-4 font-semibold text-gray-500 hover:text-blue-600 cursor-pointer'>Завершен <div className='flex justify-center items-center ml-2 w-10 h-7 rounded-full bg-blue-200 '>125</div></div>
        <div className='flex justify-center items-center mr-4 font-semibold text-gray-500 hover:text-blue-600 cursor-pointer'>Все заказы<div className='flex justify-center items-center ml-2 w-7 h-7 rounded-full bg-blue-200 '>1</div></div>
         </div>
           <div className='flex w-full h-5/6'>
<ProductList/>

           </div>
           
     </div>
  </div>
  );
}
