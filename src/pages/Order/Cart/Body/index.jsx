import React from 'react';
import Search from './Search';
import Table from './Table'
export default function index() {
  return (
  <div className='w-full h-full flex-col'>
<Search/>
<Table/>
  </div>
  );
}
