import React from "react";
import configs from "../../../../configs/default";
export default function index() {
  return (
    <div className="w-full sticky top-0 z-50">
      <div className="flex  border h-14 w-full  content-center items-center sticky top-0 z-50 bg-white">
        <div className="w-full text-2xl hover:text-blue-500 ml-10 font-semibold text-gray-700">
          Заказы
        </div>
        <div className="border-l  items-center h-full flex mr-4   text-xl cursor-pointer">
          <div className="flex text-stone-600">
            <img className="ml-3 w-20" src={configs.tаблица} alt="" />
            <div className="mr-3 ml- fill-red"> </div> Таблица <div></div>{" "}
          </div>
        </div>
        <div className="border-l w-40  items-center h-full flex mr-4   text-xl cursor-pointer">
          <div className="flex text-stone-600">
            <img className="ml-3 w-3" src={configs.Карта} alt="" />
            <div className="mr-3 ml- fill-red"> </div> Карта <div></div>{" "}
          </div>
        </div>
        <div className="border-l  items-center h-full flex mr-4   text-xl cursor-pointer">
          <div className="flex text-blue-500">
            <img className="ml-3 w-20" src={configs.Действия} alt="" />
            <div className="mr-3 ml- fill-red"> </div> Действия <div></div>{" "}
          </div>
        </div>
        <div className="border-l   items-center h-full flex mr-4 text-xl cursor-pointer">
          <div className="flex text-green-500">
            <div className="mr-3 ml-3 fill-red"> +</div> Добавить <div></div>{" "}
          </div>
        </div>
      </div>
    </div>
  );
}
