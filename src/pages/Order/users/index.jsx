import Sidebar from '../../../layouts/Sidebar'
import {Topbar} from './Topbar'
import Body from './Body'
export default function App () {

    return (
        <div className='flex w-full '>
            <Sidebar/>
            <div className="flex-col w-full">
                <Topbar/>
                <Body/>
            </div>
        
        </div>
    )
}