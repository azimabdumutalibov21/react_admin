import React from 'react'
import Search from './Search'
import Wrapper from './Wrapper'

export default function Body() {
    return (
        <div >
            <Search/>
            <Wrapper/>
        </div>
    )
}
