import React from 'react'
import config from '../../../../../configs/default'
import Customers from '../Customers/Customers'
export default function Wrapper() {
    return (
        <div className='bg-blue-200  w-full mb-0.5   px-9  py-6 h-full'>
            <div className='flex h-full'>
                <div className='flex flex-col w-full  h-full'>
                <div className=" text-black  py- rounded-lg flex   h-32  mb-6  w-full">
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr-6 transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  24
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Филиалы
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer ">
                <img src={config.home} alt="dasf" />
              </div>
            </div>
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr-6 transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  110,823
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Клиенты
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer ">
                <img src={config.user} alt="dasf" />
              </div>
            </div>
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr-6 transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  112,314
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Подписчики
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer ">
                <img src={config.ball} alt="dasf" />
              </div>
            </div>
            <div className="hover:drop-shadow-2xl flex justify-between items-center rounded-lg bg-white w-full mr- transition ease-in-out delay-150  hover:-translate-y-1 hover:scale-110 hover: duration-300">
              <div className=" ">
                <div className="ml-8 text-3xl font-bold text-blue-600">
                  73
                  <div className="text-xl font-normal text-gray-400 mt-4">
                    Курьеры
                  </div>
                </div>
              </div>
              <div className="mr-8 cursor-pointer ">
                <img src={config.car} alt="dasf" />
              </div>
            </div>
          </div>
        <Customers/>
                </div>
            </div>
        </div>
    )
}
