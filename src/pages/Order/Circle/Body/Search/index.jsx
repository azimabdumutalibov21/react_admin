import React from "react";
import { SearchIcon } from "@heroicons/react/solid";
import config from "../../../../../configs/default";
export default function Search() {
  return (
    <div className="flex bg-gray-100 border h-14 w-full  content-center items-center  ">
      <div className=" relative rounded-md ml-10 w-2/12  mr-auto ">
        <div className="absolute inset-y-4 left-0 pl-3 flex items-center pointer-events-none">
          <SearchIcon
            className="h-5 w-5 text-gray-500 group-hover:text-indigo-400"
            aria-hidden="true"
          />
        </div>
        <input
          type="text"
          className="w-full py-1 pl-10 rounded-md border-gray-100  mt-1 pl-10 pr-12  "
          placeholder="Поиск..."
          autocomplete
          autofocus
          required
        />
      </div>
      <div className=" items-center h-full flex mr-10 justify-center">
      
          <div className=" flex mr-10 bg-white h-7   justify-center rounded-md ">
            <img
              src={config.right}
              className="pr-2 p-2 border-right cursor-pointer fill-red-500 w-10 "
              alt=""
            />
            <div className="mx-3 hover:text-blue-400 ">Сегодня</div>
            <img
              src={config.left}
              className="pl-2 p-2 border-left cursor-pointer w-10 hover:bg-red"
              alt=""
            />
          </div>
       
        <div className="flex mr-10 cursor-pointer hover:text-blue-400 fill-red-400">
          <img className="mr-2" src={config.Столбцы} alt="" />{" "}
          <div>Столбцы</div>
        </div>
        <div className="flex mr-10 cursor-pointer hover:text-blue-400">
          <img className="mr-2" src={config.Фильтр} alt="" />
          <div>Фильтр</div>
        </div>
        <div className="flex cursor-pointer hover:text-blue-400">
          <img className="mr-2" src={config.Скачать} alt="" /> <div>Скачать</div>
        </div>
      </div>
    </div>
  );
}
