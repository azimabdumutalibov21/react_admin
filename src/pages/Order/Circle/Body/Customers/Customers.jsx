import React from 'react'

import Table from './table/Table'

import customerList from '../../../../../assets/boxicons-2.1.1/JsonData/customers-list.json'

const customerTableHead = [
    '',
    'Название',
    'Начало акции',
    '',
    '',
    'Конец акции',
    'Статус'
]

const renderHead = (item, index) => <th key={index}>{item}</th>

const renderBody = (item, index) => (
    <tr key={index}>
        <td>{}</td>
        <td>{item.name}</td>
        <td>{item.total_orders}</td>
        <td>{}</td>
        <td>{}</td>
        <td>{item.phone}</td>
        <td>{item.location}</td>
    </tr>
)
const Customers = () => {
    return (
        <div>
            <h2 className="page-header ">
            </h2>
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="card__body">
                            <Table
                                limit='10'
                                headData={customerTableHead}
                                renderHead={(item, index) => renderHead(item, index)}
                                bodyData={customerList}
                                renderBody={(item, index) => renderBody(item, index)}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Customers
