import React from "react";
import { Link } from "react-router-dom";
import config from '../../../../../../../configs/default'
export const Topbar = () => {
  return (
    <div className="flex w-full  border h-14  content-center items-center sticky top-0 z-50 bg-white">
      <Link to={""} className="w-full ">
        <div className="w-full text-2xl hover:text-blue-500 ml-10 font-semibold text-gray-700">
        Список акций
        </div>
      </Link>
      <div className=" w-80 border-l items-center h-full flex mr-4 text-blue-500 text-xl cursor-pointer ml-auto">
        {" "}
        <div className="mr-3 ml-3 "><img className="w-4" src={config.reff} alt="" /></div> <form action=""><button> Обновить Telegram</button> </form> 
      </div>
      <div className="border-l items-center h-full flex mr-4 text-green-500 text-xl cursor-pointer">
        {" "}
        <div className="mr-3 ml-3 fill-red">+</div> <form action="">
      Добавить </form> 
      </div>
    </div>
  );
};
