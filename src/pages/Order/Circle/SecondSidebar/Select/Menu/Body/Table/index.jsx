/* This example requires Tailwind CSS v2.0+ */
const people = [
    {
        name: 'Jane Cooper',
        title: 'Regional Paradigm Technician',
        department: 'Optimization',
        role: 'Admin',
        email: 'jane.cooper@example.com',
        image: 'https://bipbap.ru/wp-content/uploads/2017/08/fon-raduga-27502.jpg'
      },
      {
        name: 'Jane Cooper',
        title: 'Regional Paradigm Technician',
        department: 'Optimization',
        role: 'Admin',
        email: 'jane.cooper@example.com',
        image: 'https://w-dog.ru/wallpapers/15/7/424179434574168.jpg'
      },
      {
        name: 'Jane Cooper',
        title: 'Regional Paradigm Technician',
        department: 'Optimization',
        role: 'Admin',
        email: 'jane.cooper@example.com',
        image:'https://kartinkin.net/uploads/posts/2021-07/1626276566_35-kartinkin-com-p-raznie-tsveta-fon-krasivo-35.jpg'
      },
      {
        name: 'Jane Cooper',
        title: 'Regional Paradigm Technician',
        department: 'Optimization',
        role: 'Admin',
        email: 'jane.cooper@example.com',
        image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS02rn01qSWwVr3YFF3pcr2lCKkRVwPlkFsBukLoCrzu1CrHJPTVWvy8XepFeUBcibr9GQ&usqp=CAU'
      },
      {
        name: 'Jane Cooper',
        title: 'Regional Paradigm Technician',
        department: 'Optimization',
        role: 'Admin',
        email: 'jane.cooper@example.com',
        image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTriL4vS2n-9HFx50ZXiYPFReO3bRlfXqQpTFDMKVimdDiN6xAL9hoca8yXPmLwke5nQ5w&usqp=CAU'
      },
    // More people...
  ]
  
  export default function Example() {
    return (
      <div className="flex flex-col ">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200">
                <thead className="bg-gray-50">
                  <tr>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                   Изображение
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                     Hазвание на узбекском
                    </th>
                    <th
                      scope="col"
                      className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                    Hазвание на русском
                    </th>
                    <th
                      scope="col"
                      className="pl-16 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                    >
                    Статус
                    </th>
                    <th scope="col" className="relative  ">
                      <span className="sr-only">Edit</span>
                    </th>
                  </tr>
                </thead>
                <tbody className="bg-white divide-y divide-gray-200">
                  {people.map((person) => (
                    <tr key={person.email}>
                      <td className="pl-6 py- whitespace-nowrap">
                        <div className="flex items-center">
                          <div className="flex-shrink-0 flex items-center h-30 w-32">
                            <img className="h-30 w-26 rounded-xl " src={person.image} alt="" />
                          </div>
                          <div className="ml">
                            {/* <div className="text-sm font-medium text-gray-900">{person.name}</div> */}
                            {/* <div className="text-sm text-gray-500">{person.email}</div> */}
                          </div>
                        </div>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap text-xl">
                        {/* <div className="text-sm ">{person.title}</div>
                        <div className="text-sm ">{person.department}</div> */}
                        Баннер 1
                      </td>
                      <td className="px-6 py-4   ">
                        <span className="px-2 inline-flex text-xl leading-5  rounded-full  ">
                        Баннер 1
                        </span>
                      </td>
                      <td className="px-6 py-4 whitespace-nowrap text-xl "><div className="flex justify-center  rounded-xl  text-xl text-blue-500 w-40 h- bg-blue-100">Активный</div> </td>
                      <td className=" p whitespace-nowrap text-right text-sm font-medium h-20 ">
                        <div className="  ">
                        •••
                        </div>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }
  