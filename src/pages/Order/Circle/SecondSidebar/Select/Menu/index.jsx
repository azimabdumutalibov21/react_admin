import React from 'react';
import Sidebar from '../../../../../../layouts/Sidebar';
import SecondSidebar from '../../SecondSidebar';
import {Topbar} from './Topbar'
// import Search from './Search'
import Body from './Body'
export default function index() {
  return (
  <div className='w-full flex'>
      <Sidebar/>
      <div className='w-full flex'>

      <SecondSidebar/>
      <div className='flex-col w-full'>
            <Topbar/>
      <Body/>
      </div>
    
      </div>
  </div>
  );
}
