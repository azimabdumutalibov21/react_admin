import React from "react";

const ReadOnlyRow = ({ contact, handleEditClick, handleDeleteClick }) => {
  return (
    <tr className="mb-auto">
      <td className="mb-auto">{contact.fullName}</td>
      <td>{}</td>
      <td>{}</td>
      <td>{contact.phoneNumber}</td>
      <td>
        <button className="text-3xl"
          type="button"
          onClick={(event) => handleEditClick(event, contact)}
        >
        •••
        </button>
        {/* <button type="button" onClick={() => handleDeleteClick(contact.id)}>
          Delete
        </button> */}
      </td>
    </tr>
  );
};

export default ReadOnlyRow;
