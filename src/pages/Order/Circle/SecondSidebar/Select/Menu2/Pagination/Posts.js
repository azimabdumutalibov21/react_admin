import React from 'react';

const Posts = ({ posts, loading }) => {
  if (loading) {
    return <h2>Loading...</h2>;
  }

  return (
    <div className='list-grou mb-4'>
      <div className='list-group-item'></div>
      {posts.map(post => (
        <div key={post.id} className='list-group-item '>

          {post.title}

        </div>
      ))}
    </div>
  );
};

export default Posts;