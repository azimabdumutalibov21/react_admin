import React from 'react';
import Sidebar from '../../../../../../layouts/Sidebar';
import SecondSidebar from '../../SecondSidebar';
import App from './Table2'
export default function index() {
  return (
  <div className='flex w-full'>
    <Sidebar/>
    <div className='w-full flex'>

    <SecondSidebar/>
    <App/>
    </div>
    {/* <Paginate/> */}
  </div>
  );
}
