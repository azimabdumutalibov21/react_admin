import { Form, Button } from "react-bootstrap";
import { EmployeeContext } from "../contexts/EmployeeContext";
import { useContext, useState } from "react";

const AddForm = () => {
  const { addEmployee } = useContext(EmployeeContext);
  const [newEmployee, setNewEmployee] = useState({
    name: "",
    email: "",
    phone: "",
    address: "",
  });

  const onInputChange = (e) => {
    setNewEmployee({ ...newEmployee, [e.target.name]: e.target.value });
  };

  const { name, email, phone, address } = newEmployee;

  const handleSubmit = (e) => {
    e.preventDefault();
    addEmployee(name, email, phone, address);
  };
  return (
    <Form onSubmit={handleSubmit}>
      <Form.Control
        type="text"
        placeholder="Name *"
        name="name"
        value={name}
        onChange={(e) => onInputChange(e)}
        required
      ></Form.Control>
      <Form.Control
        type="email"
        placeholder="Email *"
        name="email"
        value={email}
        onChange={(e) => onInputChange(e)}
        required
      ></Form.Control>
      <Form.Control
        as="textarea"
        placeholder="Address *"
        name="address"
        rows={3}
        value={address}
        onChange={(e) => onInputChange(e)}
      ></Form.Control>
      <Form.Control
        type="text"
        placeholder="Phone *"
        name="phone"
        value={phone}
        onChange={(e) => onInputChange(e)}
        required
      ></Form.Control>
      <Button variant="success" type="submit" block>
        Add New Eployee 
      </Button>
    </Form>
  );
};
export default AddForm;
