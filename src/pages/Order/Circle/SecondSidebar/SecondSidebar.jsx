import React from "react";
import { Link, NavLink } from "react-router-dom";
import configs from "../../../../configs/default";
import Select from './Select/index'
export default function SecondSidebar() {
  return (
    <div className=" shadow  w-60">
      <div className=" px-4 pt-2 ">
        <div className="flex">
          <div className="text-3xl font-medium text-blue-500 hover:text-blue-800 cursor-pointer">
            Delever
          </div>
          <img
            className="ml-auto  w-7 cursor-pointer"
            src={configs.MenuSidebar}
            alt=""
          />
        </div>
        <div className="mt-16">
          <NavLink style={({isActive}) => ({background: isActive ? 'cyan': 'red' ,borderRadius:'4px'})} to='/circle/menu1'>
            <div className="flex items-center pl-3 font-medium  h-12 w-22 rounded-md  hover:bg-blue-500  hover:text-white ">
            <div className="">Акции</div>
          </div>
          </NavLink>
          <Link to='/circle/menu2'>
                 <div className="flex items-center pl-3 font-medium mt-1 h-12 w-22 rounded-md  hover:bg-blue-500  hover:text-white ">
            <div className="">Баннеры</div>
          </div>
          </Link>
       
        <Link to='/circle/menu3'>
         <div className="flex items-center pl-3 font-medium mt-1 h-12 w-22 rounded-md  hover:bg-blue-500  hover:text-white ">
            <div className="">Отзывы</div>
          </div>
        </Link>
         
          <div className="flex items-center  font-medium mt-1  rounded-md hover:text-white hover:bg-blue-500  ">
            <div className="">
              <Select/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
