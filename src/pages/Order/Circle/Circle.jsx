import React from 'react';
import Sidebar from '../../../layouts/Sidebar';
import SecondSidebar from './SecondSidebar/SecondSidebar';
import {Topbar} from './Topbar'
import Body from './Body'
export default function Circle() {
  return (
  <div className='flex w-full '>
  
      <Sidebar/>
<SecondSidebar/>
<div className='w-11/12  flex-col'>
  
<Topbar/>
<Body/>
</div>
  </div>
  );
}
