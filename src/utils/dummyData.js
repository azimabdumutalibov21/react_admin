export const userData = [
    {
      names:'Total orders this month',
        name: "Jan",
        "Active User": 1,
      },
      {
        name: "Feb",
        "Active User": 4,
      },
      {
        name: "Mar",
        "Active User": 1,
      },
      {
        name: "Apr",
        "Active User": 5,
      },
      {
        name: "May",
        "Active User": 2,
      },
      {
        name: "Jun",
        "Active User": 6,
      },
      {
        name: "Jul",
        "Active User": 4,
      },
      {
        name: "Aug",
        "Active User": 7,
      },
      {
        name: "Sep",
        "Active User": 1,
      },
      {
        name: "Oct",
        "Active User": 9,
      },
      {
        name: "Nov",
        "Active User": 4,
      },
      {
        name: "Dec",
        "Active User": 10,
      },
  
  ];

export const productData = [
    {
        name: "Jan",
        "Sales": 4000,
      },
      {
        name: "Feb",
        "Sales": 3000,
      },
      {
        name: "Mar",
        "Sales": 5000,
      },
    ]

  export const userRows = [
    {
      id: 1,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 2,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 3,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 4,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 5,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 6,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 7,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 8,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 9,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
    {
      id: 10,
      username: "Jon Snow",
      avatar:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRB3ceMuX4H0-RcBT3BiL4dndVvRYK98Gb-1ryc6ISnWB2zcq6lkJt3lgXCYIX9KvptzeU&usqp=CAU",
      email: "hon@gmail.com",
      status: "active",
      transaction: "$120.00",
    },
  ];

  export const productRows = [
    {
      id: 1,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 2,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 3,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 4,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 5,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 6,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 7,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 8,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 9,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
    {
      id: 10,
      name: "Jon Snow",
      img:
        "https://www.goodmorningimageshddownload.com/wp-content/uploads/2020/07/Nice-Whatsapp-DP-Images-9.jpg",
      stock: 123,
      status: "active",
      price: "$120.00",
    },
  ];