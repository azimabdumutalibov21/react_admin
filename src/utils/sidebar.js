import Generic from "../pages/Generic";


import { ReactComponent as buyurtma } from '../assets/icons/buyurtmalar.svg';
import { ReactComponent as filial } from '../assets/icons/filial.svg';
import { ReactComponent as hisobot } from '../assets/icons/hisobot.svg';
import { ReactComponent as hodimlar } from '../assets/icons/settings.svg';
import { ReactComponent as settings } from '../assets/icons/settings.svg';
import { ReactComponent as mahsulotlar } from '../assets/icons/mahsulotlar.svg';
import { ReactComponent as kategoriya } from '../assets/icons/kategoriya.svg';
import { ReactComponent as mijozlar } from '../assets/icons/mijozlar.svg';


export const sidebar = [
    {
      id: 1,
      path: '/',
      title: 'Buyurtmalar',
      component: Generic,
      Icon: buyurtma,
      hidden: false,
    },
    {
      id: 2,
      path: '/login',
      title: 'Maxsulotlar',
      component:Generic,
      Icon: mahsulotlar,
      hidden: false,
    },
    {
      id: 3,
      path: '/katigoriya',
      title: 'Katigoriya',
      component:Generic,
      Icon: kategoriya,
      hidden: false,
    },
    {
      id: 4,
      path: '/filiallar',
      title: 'Filiallar',
      component: Generic,
      Icon: filial,
      hidden: false,
    },
    {
      id: 5,
      path: '/mijozlar',
      title: 'Mijozlar',
      component: Generic,
      Icon: mijozlar,
      hidden: false,
    },
    {
      id: 6,
      path: '/hisobot',
      title: 'Hisobot',
      component: Generic,
      Icon: hisobot,
      hidden: false,
    },
    {
      id: 7,
      path: '/hodimlar',
      title: 'Hodimlar',
      component: Generic,
      Icon: hodimlar,
      hidden: false,
    },
    {
      id: 8,
      path: '/katalog',
      title: 'Katalog',
      component: Generic,
      Icon: settings,
      hidden: false,
    },
  ];