import Logo from '../assets/images/imac.png'
import Page from '../assets/images/404 Page.svg'
import Ellipse from '../assets/images/Ellipse 5.svg'
import Home from '../assets/icons/Frame.svg'
import Users from '../assets/icons/Users.svg'
import Ball from '../assets/icons/Ball.svg'
import Car from '../assets/icons/Car.svg'
import Percent from '../assets/icons/Percent.svg'
import P40 from '../assets/icons/P.svg'
import Little  from '../assets/icons/Little ball.svg'
import Left  from '../assets/icons/left.svg'
import Right  from '../assets/icons/right.svg'
import Столбцы  from '../assets/icons/Столбцы.svg'
import Фильтр from '../assets/icons/Фильтр.svg'
import Скачать from '../assets/icons/Скачать.svg'
import Arrow from '../assets/icons/arrow.svg'
import ArrowLeft from '../assets/icons/arrowLeft.svg'
import File1 from '../assets/icons/file1.svg'
import File2 from '../assets/icons/file2.svg'
import Cart from '../assets/icons/cart.svg'
import men from '../assets/icons/2.svg'
import Circle from '../assets/icons/Circle.svg'
import Nine from '../assets/icons/9.svg'
import Setting from '../assets/icons/Setting.svg'
import D from '../assets/images/logo.svg'
import Таблица from '../assets/icons/Таблица.svg'
import Карта from '../assets/icons/Карта.svg'
import Действия from '../assets/icons/Действия.svg'
import MenuSidebar from '../assets/icons/MenuSidebar.svg'
import Reff from '../assets/icons/Reff.svg'
import Action from '../assets/icons/action.svg'
import Frame from '../assets/icons/Frame3.svg'
const project = {
    welcome: '',
    project: 'Safia',
    logo: Logo,
    page: Page,
    home: Home,
    user: Users,
    ball: Ball,
    car: Car,
    parcent: Percent,
    p40: P40,
    ellipse: Ellipse,
    little: Little,
    left: Left,
    right:Right,
    Столбцы:Столбцы,
    Фильтр:Фильтр,
    Скачать:Скачать,
    arrow:Arrow,
    file1:File1,
    arrowLeft: ArrowLeft,
    file2:File2,
    cart:Cart,
    men:men,
    circle:Circle,
    nine:Nine,
    setting:Setting,
    d:D,
    tаблица:Таблица,
    Карта:Карта,
    Действия:Действия,
    MenuSidebar:MenuSidebar,
    reff:Reff,
    Action:Action,
    frame:Frame,
}


export default project